#!/usr/bin/env make -f
# Copyright 2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>

RULES_FILE := 70-xrhardware.rules
HWDB_FILE := 70-xrhardware.hwdb
HWDB_RULES_FILE := 70-xrhardware-hwdb.rules

all: $(RULES_FILE) $(HWDB_FILE) $(HWDB_RULES_FILE)
.PHONY: all


$(RULES_FILE): make-udev-rules.py xrhardware_common.py
	./$< > $@
$(HWDB_FILE): make-hwdb-file.py xrhardware_common.py
	./$< > $@
$(HWDB_RULES_FILE): make-hwdb-rules-file.py xrhardware_common.py
	./$< > $@

clean:
	-rm -f $(RULES_FILE)

.PHONY: clean

# Override if you want it in /etc/udev/rules.d or something
RULES_DIR ?= /lib/udev/rules.d

install: $(RULES_FILE)
	install -d $(DESTDIR)$(RULES_DIR)/
	install -m 644 -D $(RULES_FILE) $(DESTDIR)$(RULES_DIR)/

.PHONY: install
