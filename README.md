# XR Hardware Rules

A simple set of scripts to generate udev rules for user access to XR (VR and AR)
hardware devices.

Packages are developed over at
<https://salsa.debian.org/rpavlik-guest/xr-hardware> rather than this repo, in
an attempt to do the Debian stuff correctly by the book.

## "Build" requirements (to re-generate files)

You'll need Python 3 and the `python3-attr` Debian package or equivalent
(`pip3 install attr`, etc.)

## Build instructions

These will be elaborated upon later.

```sh
make

sudo make install  # to install udev rules to /lib/udev/rules.d - for packagers, etc.

# or

sudo make install RULES_DIR=/etc/udev/rules.d  # to install udev rules to /etc/udev/rules.d
```

`DESTDIR` is obeyed.

Right now we're generating the hwdb files as well as the shorter associated
rules file, but not doing anything with it in the makefile.

You'll need to at least re-plug your devices to get this to register for now.

## License

Boost Software License 1.0
