#!/usr/bin/env python3
# Copyright 2019-2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""Make a standalone .rules file."""

from xrhardware_common import (RULES_TEMPLATE_PREFIX, RULES_TEMPLATE_SUFFIX,
                               generate_file_contents, get_devices)

if __name__ == "__main__":
    all_rules = '\n\n'.join(d.make_commented_rule() for d in get_devices())
    contents = '\n'.join((
        RULES_TEMPLATE_PREFIX,
        '# BEGIN DEVICE LIST #',
        '#####################',
        all_rules,
        '###################',
        '# END DEVICE LIST #',
        RULES_TEMPLATE_SUFFIX
    ))
    print(generate_file_contents(__file__, contents))
