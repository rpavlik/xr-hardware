#!/usr/bin/env python3
# Copyright 2019-2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""Make a .rules file for use with a hwdb file."""

from xrhardware_common import (RULES_TEMPLATE_PREFIX, RULES_TEMPLATE_SUFFIX,
                               generate_file_contents)

if __name__ == "__main__":
    contents = '\n'.join((
        RULES_TEMPLATE_PREFIX,
        '# This rules file must be used with the corresponding hwdb file',
        RULES_TEMPLATE_SUFFIX
    ))

    # Correct name in header
    contents = contents.replace(
        "xrhardware.rules", "xrhardware-hwdb.rules")
    print(generate_file_contents(__file__, contents))
